import 'dart:math';

import 'package:flutter/material.dart';

class DialPainter extends CustomPainter {
  static const double _kTwoPi = 2 * pi;
  static const double _kPiByTwo = pi / 2;

  final List<TextPainter> labels;
  final Color backgroundColor;
  final Color accentColor;
  final double theta;
  final TextDirection textDirection;
  final int selectedValue;
  final BuildContext context;

  final double pct;
  final int multiplier;
  final int minuteHand;

  DialPainter({
    required this.labels,
    required this.backgroundColor,
    required this.accentColor,
    required this.theta,
    required this.textDirection,
    required this.selectedValue,
    required this.context,
    required this.pct,
    required this.multiplier,
    required this.minuteHand,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final double radius = size.shortestSide / 2.0;
    final Offset center = new Offset(size.width / 2.0, size.height / 2.0);
    final Offset centerPoint = center;

    print('radius: $radius');
    print('center: $center');
    print('centerPoint: $centerPoint');

    _drawOuterRingBackground(canvas, centerPoint, radius);
    _drawTranslucentCircleForEveryHour(canvas, centerPoint, radius);
    _drawInnerRingBackground(canvas, centerPoint, radius);
    _drawHandle(canvas, center, radius);
    _drawTextWhichDisplaysHoursMins(canvas, size, centerPoint);
    _drawArcAroundCircleForAmountOfCircleThatHasElapsed(
        canvas, centerPoint, radius);
    paintLables(canvas, labels, radius, center);
  }

  // Get the offset point for an angle value of theta, and a distance of _radius
  Offset _getOffsetForTheta(double theta, double _radius, Offset center) {
    return center + new Offset(_radius * cos(theta), -_radius * sin(theta));
  }

  void _drawOuterRingBackground(
      Canvas canvas, Offset centerPoint, double radius) {
    canvas.drawCircle(centerPoint, radius, Paint()..color = backgroundColor);
  }

  void _drawTranslucentCircleForEveryHour(
      Canvas canvas, Offset centerPoint, double radius) {
    print('multiplier: $multiplier');

    for (int i = 0; i < multiplier; i++) {
      double opacity = i == 0 ? 0.3 : 0.1;
      canvas.drawCircle(
        centerPoint,
        radius,
        Paint()..color = accentColor.withOpacity(opacity),
      );
    }
  }

  void _drawInnerRingBackground(
      Canvas canvas, Offset centerPoint, double radius) {
    canvas.drawCircle(
      centerPoint,
      radius,
      Paint()..color = Theme.of(context).canvasColor,
    );
  }

  void _drawHandle(Canvas canvas, Offset center, double radius) {
    final Paint handlePaint = Paint()..color = accentColor;
    final Offset handlePoint = _getOffsetForTheta(theta, radius - 10.0, center);

    canvas.drawCircle(handlePoint, 20.0, handlePaint);
  }

  void _drawTextWhichDisplaysHoursMins(
      Canvas canvas, Size size, Offset centerPoint) {
    final String hours = (multiplier == 0) ? '' : '${multiplier}h ';
    final String minutes = '$minuteHand';

    final TextPainter textDurationValuePainter = TextPainter(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: '$hours$minutes',
        style: Theme.of(context)
            .textTheme
            .headline3!
            .copyWith(fontSize: size.shortestSide * 0.15),
      ),
      textDirection: TextDirection.ltr,
    )..layout();

    Offset middelForValueText = Offset(
      centerPoint.dx - (textDurationValuePainter.width / 2),
      centerPoint.dy - (textDurationValuePainter.height / 2),
    );

    textDurationValuePainter.paint(canvas, middelForValueText);

    final TextPainter textMinPainter = TextPainter(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: 'min.',
        style: Theme.of(context).textTheme.bodyText2,
      ),
      textDirection: TextDirection.ltr,
    )..layout();

    textMinPainter.paint(
      canvas,
      Offset(
        centerPoint.dx - (textMinPainter.width / 2),
        centerPoint.dy +
            (textDurationValuePainter.height / 2) -
            (textMinPainter.height / 2),
      ),
    );
  }

  void _drawArcAroundCircleForAmountOfCircleThatHasElapsed(
      Canvas canvas, Offset centerPoint, double radius) {
    const double epsilon = .001;
    const double sweep = _kTwoPi - epsilon;
    const double startAngle = -pi / 2.0;
    final double pctTheta = (0.25 - (theta % _kTwoPi) / _kTwoPi) % 1.0;

    final Paint elapsedPainter = Paint()
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..color = accentColor.withOpacity(0.3)
      ..isAntiAlias = true
      ..strokeWidth = radius * 0.12;

    canvas.drawArc(
      Rect.fromCircle(
        center: centerPoint,
        radius: radius - radius * 0.12 / 2,
      ),
      startAngle,
      sweep * pctTheta,
      false,
      elapsedPainter,
    );
  }

  void paintLables(
      Canvas canvas, List<TextPainter> labels, double radius, Offset center) {
    if (labels == null) {
      return;
    }
    final double labelThetaIncrement = -_kTwoPi / labels.length;
    double labelTheta = _kPiByTwo;

    for (TextPainter label in labels) {
      final Offset labelOffset =
          Offset(-label.width / 2.0, -label.height / 2.0);
      label.paint(
        canvas,
        _getOffsetForTheta(labelTheta, radius - 40, center) + labelOffset,
      );

      labelTheta += labelThetaIncrement;
    }
  }

  @override
  bool shouldRepaint(DialPainter oldPainter) {
    // return false;
    return oldPainter.labels != labels ||
        oldPainter.backgroundColor != backgroundColor ||
        oldPainter.accentColor != accentColor ||
        oldPainter.theta != theta;
  }
}

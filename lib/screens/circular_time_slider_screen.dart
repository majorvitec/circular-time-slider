import 'package:circular_time_slider/widgets/circular_time_slider.dart';
import 'package:flutter/material.dart';

class CircularTimeSliderScreen extends StatefulWidget {
  @override
  _CircularTimeSliderScreenState createState() =>
      _CircularTimeSliderScreenState();
}

class _CircularTimeSliderScreenState extends State<CircularTimeSliderScreen> {
  Duration _duration = Duration(hours: 0, minutes: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: CircularTimeSlider(
                duration: _duration,
                onChanged: (val) {
                  this.setState(() => _duration = val);
                },
                snapToMins: 5.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

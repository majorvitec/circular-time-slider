# Circular Time Slider [2021]

Created a Circular Time Slider App [Followed Tutorial]

## Source
[Duration Picker for Flutter](https://pub.dev/documentation/flutter_duration_picker/latest/)

## App Overview

### Circular Slider Screen

![Circular Slider Screen](/images/readme/circular_slider_screen.png "Circular Slider Screen")
